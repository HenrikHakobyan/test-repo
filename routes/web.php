<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Home');
})->name('home');
Route::get('/Register page',function (){
    return view('Register');
})->name('Register page');
Route::post('/Register','RegisterController@Create')->name('Register');
Route::get('/Login_page',function (){
    return view('Login');
})->name('Login_page');
Route::post('/Login','LoginController@index')->name('Login');
Route::group(['middleware' => 'auth'],function (){
    Route::get('/Profile',function (){
        return view('Profile');
    })->name('Profile');
    Route::post('/Profile','RegisterController@Update_avatar')->name('Update_avatar');
    Route::get('/Post','PostController@paginate')->name('Post');
    Route::get('/Create_post',function (){
        return view('CreatePost');
    })->name('CreatePostPage');
    Route::post('CreatePost','PostController@CreatePost')->name('CreatePost');
    Route::get('/LogOut','LoginController@Logout')->name('LogOut');
});

