<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|min:2|max:10',
            'lastname' => 'required|min:4|max:15',
            'email' => 'required|email|unique:users',
            'birth_year' => 'required',
            'phone' => 'required|phone:AM',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required'
        ];
    }
    public function messages()
    {
        return array(
            'phone' => 'Phone field is not write'
        );
    }

}
