<?php

namespace App\Http\Controllers;
use Image;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Http\Requests\User\CreateUserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class registerController extends Controller
{
    /**
     * @param Request $request
     */
//    User Create
    public function create(CreateUserRequest $request){
          User::create([
          'firstname' => $request->input('firstname'),
          'lastname' => $request->input('lastname'),
          'email' => $request->input('email'),
          'birth_year' => $request->input('birth_year'),
          'phone' => $request->input('phone'),
          'password' => Hash::make($request->input('password')),
      ]);
        return redirect()->route('Login_page')->with('success','You are successfully registered');
    }
    //Handle the user upload of avatar
    public function Update_avatar(Request $request){
        if($request->hasFile('avatar')){
         $avatar = $request->file('avatar');
         $filename = time().'.'. $avatar->getClientOriginalExtension();
         Image::make($avatar)->resize(300,300)->save(public_path('/storage/avatar/'.$filename));
         $user = Auth::User();
         $user->avatar = $filename;
         $user->save();
        }
        return redirect()->route('Profile');
    }
}
