@extends('Layouts/app')
@section('title')
    Create Post
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="offset-md-4 col-md-4">
                <h1>Create Post</h1>
                <form action="{{ route('CreatePost') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputtitle">Title</label>
                        @error('title')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                        <input type="text" name="title" class="form-control" id="exampleInputtitle" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                        <label for="image">Image</label>
                        @error('image')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                        <div class="custom-file">
                            <input type="file" name="image" class="custom-file-input" id="customFileLang" lang="ru">
                            <label class="custom-file-label" for="customFileLang">Choose image</label>
                        </div>
                        @error('description')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                        <label for="description" class="mt-2">Description</label>
                        <textarea name="description" id="description" class="mt-3" cols="47" rows="3"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Send</button>
                </form>
            </div>
        </div>
    </div>
@endsection
